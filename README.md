# XFYExtension

[![CI Status](https://img.shields.io/travis/leonazhu/XFYExtension.svg?style=flat)](https://travis-ci.org/leonazhu/XFYExtension)
[![Version](https://img.shields.io/cocoapods/v/XFYExtension.svg?style=flat)](https://cocoapods.org/pods/XFYExtension)
[![License](https://img.shields.io/cocoapods/l/XFYExtension.svg?style=flat)](https://cocoapods.org/pods/XFYExtension)
[![Platform](https://img.shields.io/cocoapods/p/XFYExtension.svg?style=flat)](https://cocoapods.org/pods/XFYExtension)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYExtension is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYExtension'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYExtension is available under the MIT license. See the LICENSE file for more info.
