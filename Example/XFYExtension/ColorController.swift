//
//  ColorController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class ColorController: UIViewController {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var colorView1: UIView!
    @IBOutlet weak var colorView2: UIView!
    
    private let color1: UIColor = UIColor(red: 105.0/255, green: 105.0/255, blue: 105.0/255, alpha: 0.2)
    private let color2: UIColor = .blue
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func changeColorRate(_ sender: UISlider) {
        sender.tintColor = .transform(currentColor: color1, nextColor: color2, rate: CGFloat(sender.value))
    }
}

// MARK: - 私有
private extension ColorController {
    
    func initView() {
        label1.text = "666666"
        label1.textColor = UIColor(hexString: "666666")
        
        label2.text = "ff5577"
        label2.textColor = UIColor(hexString: "ff5577")
        
        label3.text = "ff5577 50%透明度"
        label3.textColor = UIColor(hexString: "ff5577", alpha: 0.5)
        
        colorView1.backgroundColor = color1
        colorView2.backgroundColor = color2
    }
}
