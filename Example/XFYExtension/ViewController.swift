//
//  ViewController.swift
//  XFYExtension
//
//  Created by leonazhu on 03/26/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
@_exported import XFYExtension

class ViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    private var items: [(title: String, segue: String)] = [
        ("UImageView+Extension", "UseUImageViewController"),
        ("UIDevice+Extension", "DeviceController"),
        ("UIColor+Extension", "ColorController"),
        ("Date+Extension", "DateController"),
        ("UIButton+Extension", "UseUIButtonController"),
        ("UIView+Extension", "UseUIViewController"),
        ("UIImage+Extension", "UseUIImageController"),
        ("Bundle+Extension", "BundleController"),
        ("String+Extension", "StringController"),
        ("Operator+Extension", "OperatorController"),
        ("UIStoryboard+Extension", "UseStoryboardController"),
        ("UINavigationController+Extension", "UseNavigationController")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: 列表
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "ItemCell")
        let item = items[indexPath.row]
        cell.textLabel?.text = item.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        performSegue(withIdentifier: item.segue, sender: nil)
    }
}
