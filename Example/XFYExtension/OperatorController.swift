//
//  OperatorController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/28.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class OperatorController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension OperatorController {
    
    func initView() {
        let A: [String: String] = ["第一个": "a", "哈哈": "b"]
        let B: [String: String] = ["发发发": "c", "哈哈滴滴": "d", "咦哟": "e"]
        label.text =
        """
        A:\(A)
        B:\(B)
        A <+ B:\(A <+ B)
        """
    }
}
