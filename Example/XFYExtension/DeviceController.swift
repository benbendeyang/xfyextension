//
//  DeviceController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class DeviceController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension DeviceController {
    
    func initView() {
        label.text =
        """
        当前设备：\(UIDevice.current.deviceType)
        是否iPhone：\(UIDevice.current.isPhone)
        是否iPad：\(UIDevice.current.isPad)
        是否存在安全区：\(UIDevice.current.hasSafeArea)
        """
    }
}
