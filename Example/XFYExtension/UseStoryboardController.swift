//
//  UseStoryboardController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/4/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class UseStoryboardController: UIViewController {
    
    @IBAction func pushNewController(_ sender: Any) {
        let controller = StoryboardDemoController.fromStoryboard()
        navigationController?.pushViewController(controller, animated: true)
    }
}
