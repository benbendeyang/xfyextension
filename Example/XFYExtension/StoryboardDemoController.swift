//
//  StoryboardDemoController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/4/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class StoryboardDemoController: UIViewController, FromMainStoryboard {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func createXibView(_ sender: UIButton) {
        let xibView = XibDemoView.loadFromNib()
        xibView.center = CGPoint(x: sender.center.x, y: sender.center.y + xibView.bounds.height)
        view.addSubview(xibView)
    }
}

// MARK: - 私有
private extension StoryboardDemoController {
    
    func initView() {
        label.text =
        """
        我可以快捷创建，步骤如下：
        1.创建一个协议继承“FromStoryboard”，请看“FromMainStoryboard”如何实现
        2.协议的“storyboardName”返回相关的storyboard名称
        3.需要快捷创建的控制器在storyboard中需要设置关联相应的类以及填入StoryboardID,ID与类名一致
        4.类实现中要遵循“1”中创建的协议
        5.使用时通过“类名.fromStoryboard()”方法即可快捷创建
        """
    }
}
