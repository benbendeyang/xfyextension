//
//  FromMainStoryboard.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/4/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation

protocol FromMainStoryboard: FromStoryboard {}

extension FromMainStoryboard where Self: UIViewController {
    static var storyboardName: String {
        return "Main"
    }
}
