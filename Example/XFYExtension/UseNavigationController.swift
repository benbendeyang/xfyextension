//
//  UseNavigationController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/5/21.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class UseNavigationController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension UseNavigationController {
    
    func initView() {
        navigationController?.backGesturePrior(scrollView.panGestureRecognizer)
    }
}
