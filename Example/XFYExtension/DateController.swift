//
//  DateController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class DateController: UIViewController {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    private var time = Date(timeIntervalSinceNow: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func subtractHour(_ sender: Any) {
        if let newTime = time.offsetDate(component: .hour, byAdding: -1) {
            time = newTime
            refreshView()
        }
    }
    @IBAction func addDay(_ sender: Any) {
        if let newTime = time.offsetDate(component: .day, byAdding: 1) {
            time = newTime
            refreshView()
        }
    }
}

// MARK: - 私有
private extension DateController {
    
    func initView() {
        refreshView()
    }
    
    func refreshView() {
        label1.text = "时间戳(秒级)：\(time.timeStamp)"
        label2.text = "时间戳(毫秒级)：\(time.milliStamp)"
        label3.text =
        """
        时间：\(time.formatter())
        是否今天：\(time.isToday)
        是否明天：\(time.isTomorrow)
        是否昨天：\(time.isYesterday)
        是否今年：\(time.isThisYear)
        年份：\(time.year)
        月份：\(time.month)
        日：\(time.day)
        """
    }
}
