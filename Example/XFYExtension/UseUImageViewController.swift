//
//  UseUImageViewController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class UseUImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var animationImageView: UIImageView!
    @IBOutlet weak var gifImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension UseUImageViewController {
    
    func initView() {
        imageView.setImage(url: "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg")
        
        var images: [UIImage] = []
        (0...18).forEach { i in
            if let image = UIImage(named: "loading\(i)") {
                images.append(image)
            }
        }
        animationImageView.setImages(images: images, duration: 2)
        
        guard let bundlePath = Bundle.main.path(forResource: "pikaqiu", ofType: "gif") else { return }
        gifImageView.loadGif(bundlePath)
    }
}
