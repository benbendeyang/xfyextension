//
//  BundleController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/28.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class BundleController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension BundleController {
    
    func initView() {
        label.text =
        """
        applicationName:\(Bundle.main.applicationName ?? "无")
        bundleName:\(Bundle.main.bundleName ?? "无")
        bundleVersion:\(Bundle.main.bundleVersion ?? "无")
        bundleShortVersion:\(Bundle.main.bundleShortVersion ?? "无")
        bundleExecuteable:\(Bundle.main.bundleExecuteable ?? "无")
        """
    }
}
