//
//  StringController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/28.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class StringController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func clickRight(_ sender: Any) {
        let width = view.bounds.width - 16 * 2
        let label1Size = label1.text?.size(maxWidth: width, maxHeight: view.bounds.height, font: label1.font) ?? .zero
        let view1 = UIView()
        view1.backgroundColor = .init(white: 0, alpha: 0.2)
        view1.frame = CGRect(origin: label1.frame.origin, size: label1Size)
        stackView.addSubview(view1)
        
        let label2Height = label2.text?.height(width: width, font: label2.font) ?? 0
        let view2 = UIView()
        view2.backgroundColor = .init(white: 0, alpha: 0.2)
        view2.frame = CGRect(origin: label2.frame.origin, size: CGSize(width: 10, height: label2Height))
        stackView.addSubview(view2)
        
        let label3Width = label3.text?.width(height: label3.bounds.height, font: label3.font) ?? 0
        let view3 = UIView()
        view3.backgroundColor = .init(white: 0, alpha: 0.2)
        view3.frame = CGRect(origin: label3.frame.origin, size: CGSize(width: label3Width, height: 10))
        stackView.addSubview(view3)
        
        let label4Width = label4.text?.width(font: label4.font) ?? 0
        let view4 = UIView()
        view4.backgroundColor = .init(white: 0, alpha: 0.2)
        view4.frame = CGRect(origin: label4.frame.origin, size: CGSize(width: label4Width, height: label4.bounds.height))
        stackView.addSubview(view4)
        
        label5.text =
        """
        label1的Size：\(label1Size)
        label2的height：\(label2Height.decimalsFormat)
        label3的width：\(label3Width.decimalsFormat)
        label4的width(富文本不适用)：\(label4Width.decimalsFormat)
        """
        let label5Size = label5.text?.size(maxWidth: width, maxHeight: view.bounds.height, font: label5.font) ?? .zero
        let view5 = UIView()
        view5.backgroundColor = .init(white: 0, alpha: 0.2)
        view5.frame = CGRect(origin: label5.frame.origin, size: label5Size)
        stackView.addSubview(view5)
    }
}

// MARK: - 私有
private extension StringController {
    
    func initView() {
        let str1 = "这是个什么？"
        label1.text =
        """
        \(str1)
        首字母：\(str1.firstLetterFromString)
        是否为空：\(str1.isBlank())
        是否为手机号码：\(str1.isValidMobilePhone)
        隐藏中间号码：\(str1.replacePhone ?? "不是手机号码无法隐藏")
        是否为身份证：\(str1.checkIdentityCardNumber)
        """
        
        let str2 = "13976100163"
        label2.text =
        """
        \(str2)
        首字母 = \(str2.firstLetterFromString)
        是否为空：\(str2.isBlank())
        是否为手机号码：\(str2.isValidMobilePhone)
        隐藏中间号码：\(str2.replacePhone ?? "不是手机号码无法隐藏")
        是否为身份证：\(str2.checkIdentityCardNumber)
        """
        
        let str3 = "蜀道之难难于上青天！"
        let idCardNumber = "51192219860417121X"
        let version1 = "1.12.1"
        let version2 = "1.2.13"
        var versionCompare = "无法对比"
        if let isGreater = version1.isGreater(to: version2, separateStr: ".") {
            versionCompare = isGreater ? ">" : "<"
        }
        label3.text =
        """
        \(str3)
        正则替换，\(str3.pregReplace(pattern: "青天", with: "厕所"))
        正则替换，\(str3.pregReplace(pattern: "难", with: "*"))
        \(idCardNumber)是否为身份证：\(idCardNumber.checkIdentityCardNumber)
        \(version1)\(versionCompare)\(version2)
        """
        
        let str4 = "今天天气特别好"
        label4.attributedText = str4.addAttribute(to: "特别", color: .red, font: .systemFont(ofSize: 20))
    }
}
