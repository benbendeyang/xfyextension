//
//  XibDemoView.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/4/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class XibDemoView: UIView, NibLoadable {

    @IBOutlet weak var textView: UITextView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
