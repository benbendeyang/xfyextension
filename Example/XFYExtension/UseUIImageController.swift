//
//  UseUIImageController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class UseUIImageController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    private let image = #imageLiteral(resourceName: "loading1")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: 操作
    @IBAction func restore(_ sender: Any) {
        imageView.image = image
        imageView2.image = image
    }
    @IBAction func clickButton1(_ sender: Any) {
        imageView.image = image.resizeImage(maxLength: 200)
    }
    @IBAction func clickButton2(_ sender: Any) {
        imageView.image = image.resizeImage(newSize: CGSize(width: 300, height: 160))
    }
    @IBAction func changeRedColor(_ sender: Any) {
        imageView2.image = image.imageWithTintColor(color: .red)
    }
    @IBAction func changeBlueColor(_ sender: Any) {
        imageView2.image = image.imageWithTintColor(color: .blue)
    }
    @IBAction func changeBlackColor(_ sender: Any) {
        imageView2.image = image.imageWithTintColor(color: .black)
    }
}
