//
//  UseUIButtonController.swift
//  XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class UseUIButtonController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: 操作
    @IBAction func clickButton(_ sender: Any) {
        button.countDown(count: 8, normal: "我是一个倒计时按钮", decrease: "剩余/s")
    }
}
