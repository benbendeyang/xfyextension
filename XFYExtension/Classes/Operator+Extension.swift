//
//  Operator+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/28.
//

infix operator <+ : Precedence
precedencegroup Precedence {
    associativity: left
    higherThan: BitwiseShiftPrecedence
}
/// 追加字典
public func <+<K, V>(left: [K: V], right: [K: V]) -> [K: V] {
    
    return right.compactMap { $0 }.reduce(left) { (result, tuple) -> [K: V] in
        var newResult = result
        newResult.updateValue(tuple.1, forKey: tuple.0)
        return newResult
    }
}
