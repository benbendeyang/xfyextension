//
//  Double+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/5/21.
//

import Foundation

public extension Double {
    
    var decimalsFormat: String {
        let format = String(format: "%.2f", self)
        return format.decimalsFormat
    }
}
