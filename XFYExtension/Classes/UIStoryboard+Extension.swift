//
//  UIStoryboard+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/4/18.
//


import Foundation

// MARK: - Storyboard、xib快速创建
public extension UIStoryboard {
    
    class func instantiateViewController<T: UIViewController>(_ storyboardIdentifier: String = "Main") -> T {
        let storyboard = UIStoryboard(name: storyboardIdentifier, bundle: nil)
        return storyboard.instantiateViewController()
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        return instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
}

public protocol FromStoryboard {
    
    static var storyboardName: String { get }
    static func fromStoryboard() -> Self
}

public extension FromStoryboard where Self: UIViewController {
    
    static func fromStoryboard() -> Self {
        return UIStoryboard.instantiateViewController(storyboardName)
    }
}

public protocol NibLoadable {}

public extension NibLoadable where Self : UIView {
    
    static func loadFromNib(_ nibname : String? = nil) -> Self {
        let loadName = nibname == nil ? "\(self)" : nibname!
        return Bundle.main.loadNibNamed(loadName, owner: nil, options: nil)?.first as! Self
    }
}
