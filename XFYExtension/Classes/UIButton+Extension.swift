//
//  UIButton+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation

// MARK: - 倒计时
public extension UIButton {
    
    /**
     按钮倒计时
     - Authors: 🐑
     
     - Parameters:
        - count: 倒计时（秒）
        - normal: 默认文字
        - decrease: 倒计时状态文字,用"/s"代替秒数（例如："倒计/s秒")
     */
    func countDown(count: Int, normal: String, decrease: String) {
        // 倒计时开始,禁止点击事件
        isEnabled = false
        
        var remainingCount: Int = count {
            willSet {
                setTitle(decrease.replacingOccurrences(of: "/s", with: "\(newValue)"), for: .normal)
                if newValue <= 0 {
                    setTitle(normal, for: .normal)
                }
            }
        }
        
        // 在global线程里创建一个时间源
        let codeTimer = DispatchSource.makeTimerSource(queue:DispatchQueue.global())
        // 设定这个时间源是每秒循环一次，立即开始
        codeTimer.schedule(deadline: .now(), repeating: .seconds(1))
        // 设定时间源的触发事件
        codeTimer.setEventHandler(handler: {
            
            // 返回主线程处理一些事件，更新UI等等
            DispatchQueue.main.async {
                // 每秒计时一次
                remainingCount -= 1
                // 时间到了取消时间源
                if remainingCount <= 0 {
                    self.isEnabled = true
                    codeTimer.cancel()
                }
            }
        })
        // 启动时间源
        codeTimer.resume()
    }
}
