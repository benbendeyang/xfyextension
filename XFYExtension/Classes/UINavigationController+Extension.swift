//
//  UINavigationController+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/5/21.
//

import Foundation

public extension UINavigationController {
    
    func backGesturePrior(_ failGestur: UIGestureRecognizer) {
        guard let gestureArray = view.gestureRecognizers else { return }
        for gesture in gestureArray {
            if gesture.isKind(of: UIScreenEdgePanGestureRecognizer.self) {
                failGestur.require(toFail: gesture)
                break
            }
        }
    }
}
