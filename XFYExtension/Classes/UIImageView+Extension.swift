//
//  UIImageView+Extension.swift
//  Pods-XFYExtension_Example
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation
import Kingfisher

public extension UIImageView {
    
    func setImage(url: String, placeholder: Placeholder? = nil, progressBlock: DownloadProgressBlock? = nil, completionHandler: ((Result<RetrieveImageResult, KingfisherError>) -> Void)? = nil) {
        kf.cancelDownloadTask()
        kf.setImage(with: URL(string: url), placeholder: placeholder, progressBlock: progressBlock, completionHandler: completionHandler)
    }
    
    func setImages(images: [UIImage], duration: TimeInterval? = nil) {
        animationImages = images
        if let duration = duration {
            animationDuration = duration
        }
        startAnimating()
    }
    
    func loadGif(_ bundlePath: String, duration: TimeInterval? = nil) {
        
        guard let data = NSData(contentsOfFile: bundlePath),
            let imageSource = CGImageSourceCreateWithData(data, nil) else { return }
        
        var images = [UIImage]()
        var totalDuration : TimeInterval = 0
        for i in 0..<CGImageSourceGetCount(imageSource) {
            guard let cgImage = CGImageSourceCreateImageAtIndex(imageSource, i, nil) else { continue }
            let image = UIImage(cgImage: cgImage)
            i == 0 ? self.image = image : ()
            images.append(image)
            
            guard duration == nil, let properties = CGImageSourceCopyPropertiesAtIndex(imageSource, i, nil),
                let gifInfo = (properties as NSDictionary)[kCGImagePropertyGIFDictionary as String] as? NSDictionary,
                let frameDuration = (gifInfo[kCGImagePropertyGIFDelayTime as String] as? NSNumber) else
            { continue }
            totalDuration += frameDuration.doubleValue
        }
        
        if let duration = duration {
            setImages(images: images, duration: duration)
            animationDuration = duration
        } else {
            setImages(images: images, duration: totalDuration)
        }
    }
}
