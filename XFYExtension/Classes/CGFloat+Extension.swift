//
//  CGFloat+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/28.
//

import Foundation

public extension CGFloat {
    
    /// 像素计算
    var aligned: CGFloat {
        let floorSelf = floor(self)
        let diff = self - floorSelf
        let element = 1 / UIScreen.main.scale
        let diffInt = Int(diff * 10000)
        let elementInt = Int(element * 10000)
        if diffInt % elementInt == 0 {
            return self
        } else {
            return CGFloat(diffInt / elementInt + 1) * element + floorSelf
        }
    }
    
    /// 保留两位小数
    var decimalsFormat: String {
        let format = String(format: "%.2f", self)
        return format.decimalsFormat
    }
}
