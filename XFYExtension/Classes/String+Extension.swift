//
//  String+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/28.
//

import Foundation

public extension String {
    
    var utf8Encoded: Data? {
        return data(using: .utf8)
    }
    
    /// 获取拼音首字母（大写字母）
    var firstLetterFromString: String {
        // 转变成可变字符串
        let mutableString = NSMutableString.init(string: self)
        // 将中文转换成带声调的拼音
        CFStringTransform(mutableString as CFMutableString, nil, kCFStringTransformToLatin, false)
        // 去掉声调
        let pinyinString = mutableString.folding(options: String.CompareOptions.diacriticInsensitive, locale:   NSLocale.current)
        // 截取大写首字母
        let firstString = String(pinyinString.prefix(1).uppercased())
        //判断首字母是否为大写
        let regexA = "^[A-Z]$"
        let predA = NSPredicate.init(format: "SELF MATCHES %@", regexA)
        return predA.evaluate(with: firstString) ? firstString : "#"
    }
    
    /// 保留两位小数
    var decimalsFormat: String {
        var format = "\(self)"
        let strArray = format.components(separatedBy: ".")
        if strArray.count > 1 {
            if strArray[1] == "00" {
                return strArray[0]
            } else if format.hasSuffix("0") {
                format.remove(at: format.index(before: format.endIndex))
                return format
            } else {
                return format
            }
        } else {
            return format
        }
    }
}

// MARK: - 正则
public extension String {
    
    /// 正则判断
    func isRule(pattern: String) -> Bool {
        do {
            let regex: NSRegularExpression = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let matches = regex.matches(in: self, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, self.count))
            return matches.count > 0
        } catch {
            return false
        }
    }
    
    /// 使用正则表达式替换
    func pregReplace(pattern: String, with: String,
                     options: NSRegularExpression.Options = []) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: options)
        return regex.stringByReplacingMatches(in: self, options: [],
                                              range: NSMakeRange(0, count),
                                              withTemplate: with)
    }
    
    /// 是否数字+字符
    func isLetterWithDigital() -> Bool {
        let numberRegex: NSPredicate = NSPredicate(format: "SELF MATCHES %@","^.*[0-9]+.*$")
        let letterRegex:NSPredicate = NSPredicate(format: "SELF MATCHES %@","^.*[A-Za-z]+.*$")
        return (numberRegex.evaluate(with: self) && letterRegex.evaluate(with: self))
    }
    
    /// 是否为空
    func isBlank() -> Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    /// 是否为空
    static func isBlank(string: String?) -> Bool {
        if let string = string {
            return string.isBlank()
        } else {
            return true
        }
    }
}

// MARK: - 手机号码
public extension String {
    
    /// 是否为手机号码
    var isValidMobilePhone: Bool {
        return isRule(pattern: "^1[0-9]{10}$")
    }
    
    /// 隐藏中间号码
    var replacePhone: String? {
        guard isValidMobilePhone else { return nil }
        let start = index(startIndex, offsetBy: 3)
        let end = index(startIndex, offsetBy: 7)
        let range = Range(uncheckedBounds: (lower: start, upper: end))
        return replacingCharacters(in: range, with: "****")
    }
}

// MARK: - 身份证
public extension String {
    
    /// 验证身份证号
    var checkIdentityCardNumber: Bool {
        // 判断位数
        if count != 15 && count != 18 {
            return false
        }
        var carid = self
        
        var lSumQT = 0
        
        //self加权因子
        let R = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
        
        // 校验码
        let sChecker: [Int8] = [49, 48, 88, 57, 56, 55, 54, 53, 52, 51, 50]
        
        // 将15位身份证号转换成18位
        let mString = NSMutableString.init(string: self)
        
        if count == 15 {
            mString.insert("19", at: 6)
            var p = 0
            let pid = mString.utf8String
            for i in 0...16 {
                let t = Int(pid![i])
                p += (t - 48) * R[i]
            }
            let o = p % 11
            let stringContent = NSString(format: "%c", sChecker[o])
            mString.insert(stringContent as String, at: mString.length)
            carid = mString as String
        }
        
        let cStartIndex = carid.startIndex
        let cEndIndex = carid.endIndex
        let index = carid.index(cStartIndex, offsetBy: 2)
        // 判断地区码
        let sProvince = String(carid[cStartIndex..<index])
        if (!self.areaCodeAt(sProvince)) {
            return false
        }
        
        //判断年月日是否有效
        //年份
        let yStartIndex = carid.index(cStartIndex, offsetBy: 6)
        let yEndIndex = carid.index(yStartIndex, offsetBy: 4)
        let strYear = Int(carid[yStartIndex..<yEndIndex])
        
        //月份
        let mStartIndex = carid.index(yEndIndex, offsetBy: 0)
        let mEndIndex = carid.index(mStartIndex, offsetBy: 2)
        let strMonth = Int(carid[mStartIndex..<mEndIndex])
        
        //日
        let dStartIndex = carid.index(mEndIndex, offsetBy: 0)
        let dEndIndex = carid.index(dStartIndex, offsetBy: 2)
        let strDay = Int(carid[dStartIndex..<dEndIndex])
        
        let localZone = NSTimeZone.local
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.timeZone = localZone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: "\(String(format: "%02d",strYear!))-\(String(format: "%02d",strMonth!))-\(String(format: "%02d",strDay!)) 12:01:01")
        
        if date == nil {
            return false
        }
        let paperId = carid.utf8CString
        // 检验长度
        if 18 != carid.count {
            return false
        }
        // 校验数字
        func isDigit(c: Int) -> Bool {
            return 0 <= c && c <= 9
        }
        for i in 0...18 {
            let id = Int(paperId[i])
            if isDigit(c: id) && !(88 == id || 120 == id) && 17 == i {
                return false
            }
        }
        // 验证最末的校验码
        for i in 0...16 {
            let v = Int(paperId[i])
            lSumQT += (v - 48) * R[i]
        }
        if sChecker[lSumQT%11] != paperId[17] {
            return false
        }
        return true
    }
    
    /// 身份证区域码判断
    func areaCodeAt(_ code: String) -> Bool {
        var dic: [String: String] = [:]
        dic["11"] = "北京"
        dic["12"] = "天津"
        dic["13"] = "河北"
        dic["14"] = "山西"
        dic["15"] = "内蒙古"
        dic["21"] = "辽宁"
        dic["22"] = "吉林"
        dic["23"] = "黑龙江"
        dic["31"] = "上海"
        dic["32"] = "江苏"
        dic["33"] = "浙江"
        dic["34"] = "安徽"
        dic["35"] = "福建"
        dic["36"] = "江西"
        dic["37"] = "山东"
        dic["41"] = "河南"
        dic["42"] = "湖北"
        dic["43"] = "湖南"
        dic["44"] = "广东"
        dic["45"] = "广西"
        dic["46"] = "海南"
        dic["50"] = "重庆"
        dic["51"] = "四川"
        dic["52"] = "贵州"
        dic["53"] = "云南"
        dic["54"] = "西藏"
        dic["61"] = "陕西"
        dic["62"] = "甘肃"
        dic["63"] = "青海"
        dic["64"] = "宁夏"
        dic["65"] = "新疆"
        dic["71"] = "台湾"
        dic["81"] = "香港"
        dic["82"] = "澳门"
        dic["91"] = "国外"
        if (dic[code] == nil) {
            return false
        }
        return true
    }
}

// MARK: - 富文本
public extension String {
    
    func addAttributes(to attributeStr: String, attrs: [NSAttributedString.Key : Any]) -> NSMutableAttributedString {
        let attriStr: NSMutableAttributedString = NSMutableAttributedString(string: self)
        let range = NSMakeRange(NSString(string: self).range(of: attributeStr).location, NSString(string: self).range(of: attributeStr).length)
        attriStr.addAttributes(attrs, range: range)
        return attriStr
    }
    
    func addAttribute(to attributeStr: String, color: UIColor? = nil, font: UIFont? = nil) -> NSMutableAttributedString {
        let attriStr: NSMutableAttributedString = NSMutableAttributedString(string: self)
        let range = NSMakeRange(NSString(string: self).range(of: attributeStr).location, NSString(string: self).range(of: attributeStr).length)
        if let color = color {
            attriStr.addAttribute(.foregroundColor, value: color, range: range)
        }
        if let font = font {
            attriStr.addAttribute(.font, value: font, range: range)
        }
        return attriStr
    }
}

// MARK: - 字符串宽度计算
public extension String {
    
    func size(maxWidth: CGFloat, maxHeight: CGFloat, font: UIFont) -> CGSize {
        let constraintedSize = CGSize(width: maxWidth, height: maxHeight)
        var rect = self.boundingRect(with: constraintedSize, options: [.usesLineFragmentOrigin,.usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        rect.size.height = rect.size.height.aligned
        rect.size.width = rect.size.width.aligned
        return rect.size
    }
    
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        return size(maxWidth: width, maxHeight: UIScreen.main.bounds.height, font: font).height
    }
    
    func width(height: CGFloat, font: UIFont) -> CGFloat {
        return size(maxWidth: UIScreen.main.bounds.width, maxHeight: height, font: font).width
    }
    
    func height(width: CGFloat, fontSize: CGFloat) -> CGFloat {
        return height(width: width, font: UIFont.systemFont(ofSize: fontSize))
    }
    
    func height(width: CGFloat, boldFontSize: CGFloat) -> CGFloat {
        return height(width: width, font: UIFont.boldSystemFont(ofSize: boldFontSize))
    }
    
    func heightInPaddingToScreen(_ paddingToLeftOrRight: CGFloat, font: UIFont) -> CGFloat {
        return self.height(width: UIScreen.main.bounds.width - 2 * paddingToLeftOrRight, font: font)
    }
    
    func heightInTotalHorizontalPaddingToScreen(_ totalPadding: CGFloat, font: UIFont) -> CGFloat {
        return self.height(width: UIScreen.main.bounds.width - totalPadding, font: font)
    }
    
    func width(font: UIFont) -> CGFloat {
        return size(withAttributes: [NSAttributedString.Key.font: font]).width.aligned
    }
}

// MARK: - 版本
public extension String {
    
    /// 比较大小
    func isGreater(to contrast: String, separateStr: String) -> Bool? {
        let originalArray = self.components(separatedBy: separateStr)
        let contrastArray = contrast.components(separatedBy: separateStr)
        if originalArray.count > contrastArray.count {
            return true
        } else if originalArray.count < contrastArray.count {
            return false
        } else {
            for i in 0..<originalArray.count {
                guard let original = Int(originalArray[i]), let contrast = Int(contrastArray[i]) else {
                    return nil
                }
                if original > contrast {
                    return true
                } else if original < contrast {
                    return false
                }
            }
            return false
        }
    }
}
