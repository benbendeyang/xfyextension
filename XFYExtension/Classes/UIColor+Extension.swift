//
//  UIColor+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation

// MARK: - 颜色转换
public extension UIColor {
    
    convenience init?(hexString: String) {
        self.init(hexString: hexString, alpha: 1.0)
    }
    
    convenience init?(hexString: String, alpha: Float) {
        var hex = hexString
        
        // Check for hash and remove the hash
        if hex.hasPrefix("#") {
            hex = String(hex.dropFirst())
        }
        
        if (hex.range(of: "(^[0-9A-Fa-f]{6}$)|(^[0-9A-Fa-f]{3}$)", options: .regularExpression) != nil) {
            // Deal with 3 character Hex strings
            if hex.count == 3 {
                
                let redHex = String(hex.dropLast(2))
                let greenHex = String(hex.dropFirst().dropLast())
                let blueHex = String(hex.dropFirst(2))
                
                hex = redHex + redHex + greenHex + greenHex + blueHex + blueHex
            }
            
            let redHex = String(hex.dropLast(4))
            let greenHex = String(hex.dropLast(2).dropFirst(2))
            let blueHex = String(hex.dropFirst(4))
            
            var redInt: CUnsignedInt = 0
            var greenInt: CUnsignedInt = 0
            var blueInt: CUnsignedInt = 0
            
            Scanner(string: redHex).scanHexInt32(&redInt)
            Scanner(string: greenHex).scanHexInt32(&greenInt)
            Scanner(string: blueHex).scanHexInt32(&blueInt)
            
            self.init(red: CGFloat(redInt) / 255.0, green: CGFloat(greenInt) / 255.0, blue: CGFloat(blueInt) / 255.0, alpha: CGFloat(alpha))
        } else {
            self.init()
            return nil
        }
    }
    
    /// color参数需要通过rgb创建，快捷创建可能会导致返回nil
    static func transform(currentColor: UIColor, nextColor: UIColor, rate: CGFloat) -> UIColor? {
        var rgbComponents: [CGFloat] = []
        guard let currentRGBComponents = currentColor.cgColor.components, currentRGBComponents.count >= 4,
            let nextRGBComponents = nextColor.cgColor.components, nextRGBComponents.count >= 4 else {
                return nil
        }
        (0..<4).forEach { i in
            let currentValue = currentRGBComponents[i]
            let nextValue = nextRGBComponents[i]
            let value = currentValue + (nextValue - currentValue) * CGFloat(rate)
            rgbComponents.append(value)
        }
        return UIColor(red: rgbComponents[0], green: rgbComponents[1], blue: rgbComponents[2], alpha: rgbComponents[3])
    }
}
