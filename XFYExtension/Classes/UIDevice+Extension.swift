//
//  UIDevice+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation

public extension UIDevice {
    
    enum DeviceIdentifier {
        
        case unknow
        
        case iPod1  // iPod Touch 1
        case iPod2  // iPod Touch 2
        case iPod3  // iPod Touch 3
        case iPod4  // iPod Touch 4
        case iPod5  // iPod Touch (5 Gen)
        case iPod6  // iPod Touch 6
        
        case iPhone4    // iPhone 4
        case iPhone4s   // iPhone 4s
        case iPhone5    // iPhone 5
        case iPhone5c   // iPhone 5c
        case iPhone5s   // iPhone 5s
        case iPhone6    // iPhone 6
        case iPhone6Plus    // iPhone 6 Plus
        case iPhone6s       // iPhone 6s
        case iPhone6sPlus   // iPhone 6s Plus
        case iPhoneSE       // iPhone SE
        
        case iPhone7        // iPhone 7
        case iPhone7Plus    // iPhone 7 Plus
        case iPhone8        // iPhone 8
        case iPhone8Plus    // iPhone 8 Plus
        case iPhoneX        // iPhone X
        case iPhoneXs       // iPhone XS
        case iPhoneXsMax    // iPhone XS Max
        case iPhoneXr       // iPhone XR
        
        case iPad       // iPad
        case iPad2      // iPad 2
        case iPad3      // iPad 3
        case iPad4      // iPad 4
        case iPad5      // iPad 5
        case iPadMini   // iPad Mini
        case ipadMini2  // iPad Mini 2
        case ipadMini3  // iPad Mini 3
        case ipadMini4  // iPad Mini 4
        case iPadAir    // iPad Air
        case iPadAir2   // iPad Air 2
        case iPadPro    // iPad Pro
        
        case appleTV2   // Apple TV 2
        case appleTV3   // Apple TV 3
        case appleTV4   // Apple TV 4
        
        case simulator  // Simulator
    }
    
    var deviceType: DeviceIdentifier {
        
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod1,1": return .iPod1
        case "iPod2,1": return .iPod2
        case "iPod3,1": return .iPod3
        case "iPod4,1": return .iPod4
        case "iPod5,1": return .iPod5
        case "iPod7,1": return .iPod6
            
        case "iPhone3,1", "iPhone3,2", "iPhone3,3": return .iPhone4
        case "iPhone4,1":   return .iPhone4s
        case "iPhone5,1":   return .iPhone5
        case "iPhone5,2":   return .iPhone5     // GSM+CDMA
        case "iPhone5,3":   return .iPhone5c    // GSM
        case "iPhone5,4":   return .iPhone5c    // GSM+CDMA
        case "iPhone6,1":   return .iPhone5s    // GSM
        case "iPhone6,2":   return .iPhone5s    // GSM+CDMA
        case "iPhone7,2":   return .iPhone6s
        case "iPhone7,1":   return .iPhone6Plus
        case "iPhone8,1":   return .iPhone6s
        case "iPhone8,2":   return .iPhone6sPlus
        case "iPhone8,4":   return .iPhoneSE
            
        case "iPhone9,1":   return .iPhone7     // 国行、日版、港行
        case "iPhone9,2":   return .iPhone7Plus // 港行、国行
        case "iPhone9,3":   return .iPhone7     // 美版、台版
        case "iPhone9,4":   return .iPhone7     // 美版、台版
        case "iPhone10,1","iPhone10,4": return .iPhone8
        case "iPhone10,2","iPhone10,5": return .iPhone8Plus
        case "iPhone10,3","iPhone10,6": return .iPhoneX
        case "iPhone11,2":  return .iPhoneXs
        case "iPhone11,4","iPhone11,6": return .iPhoneXsMax
        case "iPhone11,8":  return .iPhoneXr
            
        case "iPad1,1": return .iPad
        case "iPad1,2": return .iPad    // 3G
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":    return .iPad2
        case "iPad2,5", "iPad2,6", "iPad2,7":   return .iPadMini
        case "iPad3,1", "iPad3,2", "iPad3,3":   return .iPad3
        case "iPad3,4", "iPad3,5", "iPad3,6":   return .iPad4
        case "iPad4,1", "iPad4,2", "iPad4,3":   return .iPadAir
        case "iPad4,4", "iPad4,5", "iPad4,6":   return .ipadMini2
        case "iPad4,7", "iPad4,8", "iPad4,9":   return .ipadMini3
        case "iPad5,1", "iPad5,2":  return .ipadMini4
        case "iPad5,3", "iPad5,4":  return .iPadAir2
        case "iPad6,3", "iPad6,4":  return .iPadPro // iPad Pro 9.7
        case "iPad6,7", "iPad6,8":  return .iPadPro // iPad Pro 12.9
        case "iPad6,11", "iPad6,12":    return .iPad5
        case "iPad7,1", "iPad7,2":  return .iPadPro // iPad Pro 12.9
        case "iPad7,3", "iPad7,4":  return .iPadPro // iPad Pro 10.5
            
        case "AppleTV2,1":  return .appleTV2
        case "AppleTV3,1","AppleTV3,2": return .appleTV3
        case "AppleTV5,3":  return .appleTV4
            
        case "i386", "x86_64":  return .simulator
            
        default:    return .unknow
        }
    }
    
    var isPad: Bool {
        return userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    
    var isPhone: Bool {
        return userInterfaceIdiom == UIUserInterfaceIdiom.phone
    }
    
    var hasSafeArea: Bool {
        let type = deviceType
        if type == .simulator {
            let ScreenHeight = UIScreen.main.bounds.height
            return (ScreenHeight == 812.0 || ScreenHeight == 896.0) && UIScreen.main.scale == 3.0
        } else {
            return (type == .iPhoneX) || (type == .iPhoneXs) || (type == .iPhoneXsMax) || (type == .iPhoneXr)
        }
    }
}
