//
//  UIImage+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation

// MARK: - 尺寸转换
public extension UIImage {
    
    /**
     通过指定图片最长边，获得等比例的图片size
     - Authors: 🐑
     - Parameters:
        - imageLength: 图片允许的最长宽度（高度）
     - Returns: 限制后的尺寸
     */
    private func scaleImage(maxLength: CGFloat) -> CGSize {
        
        var newWidth: CGFloat = 0
        var newHeight: CGFloat = 0
        let width = size.width
        let height = size.height
        
        if (width > maxLength) || (height > maxLength) {
            
            if width > height {
                newWidth = maxLength
                newHeight = newWidth * height / width
            } else if height > width {
                newHeight = maxLength
                newWidth = newHeight * width / height
            } else {
                newWidth = maxLength
                newHeight = maxLength
            }
        }
        return CGSize(width: newWidth, height: newHeight)
    }
    
    /**
     获得指定size的图片
     - Authors: 🐑
     - Parameters:
        - newSize: 指定的size
     - Returns: 调整后的图片
     */
    func resizeImage(newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let resizeImage = newImage else { return self }
        return resizeImage
    }
    
    /**
     获得指定size的图片
     - Authors: 🐑
     - Parameters:
        - maxLength: 图片允许的最长宽度（高度）
     - Returns: 调整后的图片
     */
    func resizeImage(maxLength: CGFloat) -> UIImage {
        return resizeImage(newSize: scaleImage(maxLength: maxLength))
    }
}

// MARK: - 颜色
extension UIImage {
    
    /**
     更改图片颜色
     - Authors: 🐑
     - Parameters:
        - color: 需要修改成的颜色
     - Returns: 调整后的图片
     */
    public func imageWithTintColor(color : UIColor) -> UIImage {
        UIGraphicsBeginImageContext(size)
        color.setFill()
        let bounds = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
        UIRectFill(bounds)
        draw(in: bounds, blendMode: CGBlendMode.destinationIn, alpha: 1.0)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = tintedImage else {
            return self
        }
        return image
    }
}
