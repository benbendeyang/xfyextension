//
//  Date+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/27.
//

import Foundation

public extension Date {
    
    /// 获取当前 秒级 时间戳 - 10位
    var timeStamp: String {
        let timeInterval: TimeInterval = timeIntervalSince1970
        let timeStamp = Int(timeInterval)
        return "\(timeStamp)"
    }
    
    /// 获取当前 毫秒级 时间戳 - 13位
    var milliStamp: String {
        let timeInterval: TimeInterval = timeIntervalSince1970
        let millisecond = CLongLong(round(timeInterval * 1000))
        return "\(millisecond)"
    }
    
    func formatter(dateFormat: String = "YYYY-MM-dd HH:mm:ss") -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = dateFormat
        return dateformatter.string(from: self)
    }
}

// MARK: - 常用判断
public extension Date {
    
    private var dateComponents: DateComponents {
        let calendar = Calendar.current
        let set: Set<Calendar.Component> = [.era, .year, .month, .day, .hour, .minute, .second, .weekday, .weekdayOrdinal, .quarter, .weekOfMonth, .weekOfYear, .yearForWeekOfYear, .nanosecond, .calendar, .timeZone]
        let dateComponents = calendar.dateComponents(set, from: self)
        return dateComponents
    }
    
    var year: Int {
        return dateComponents.year ?? 0
    }
    var month: Int {
        return dateComponents.month ?? 0
    }
    var day: Int {
        return dateComponents.day ?? 0
    }
    
    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isTomorrow: Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    var isYesterday: Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    var isThisYear: Bool {
        let compareResult = Calendar.current.compare(self, to: Date(), toGranularity: .year)
        return compareResult == .orderedSame
    }
    
    func isEqualTo(date: Date) -> Bool {
        return self.timeIntervalSince(date) == 0
    }
    
    func isEarlier(than date: Date) -> Bool {
        let compareResult = Calendar.current.compare(self, to: date, toGranularity: .second)
        return compareResult == .orderedAscending
    }
    
    func isEarlierorEqualTo(date: Date) -> Bool {
        guard !self.isEarlier(than: date) else { return true }
        return isEqualTo(date: date)
    }
    
    func isLater(than date: Date) -> Bool {
        let compareResult = Calendar.current.compare(self, to: date, toGranularity: .second)
        return compareResult == .orderedDescending
    }
    
    func isLaterOrEqualTo(date: Date) -> Bool {
        guard !self.isLater(than: date) else { return true }
        return isEqualTo(date: date)
    }
    
    func hours(before date: Date) -> Int {
        return Int(date.timeIntervalSince(self)) / 3600
    }
    
    func offsetDate(component: Calendar.Component, byAdding value: Int) -> Date? {
        return Calendar.current.date(byAdding: component, value: value, to: self)
    }
}
