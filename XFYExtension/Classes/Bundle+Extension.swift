//
//  Bundle+Extension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/28.
//

import Foundation

public extension Bundle {
    
    var applicationName: String? {
        
        if let name = object(forInfoDictionaryKey: "CFBundleDisplayName") as? String {
            return name
        } else {
            return bundleName
        }
    }
    
    var bundleName: String? {
        return object(forInfoDictionaryKey: String(kCFBundleNameKey)) as? String
    }
    
    var bundleVersion: String? {
        return object(forInfoDictionaryKey: String(kCFBundleVersionKey)) as? String
    }
    
    var bundleShortVersion: String? {
        return object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
    
    var bundleExecuteable: String? {
        return object(forInfoDictionaryKey: String(kCFBundleExecutableKey)) as? String
    }
}
